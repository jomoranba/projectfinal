package com.everis.notification.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class NotificationService {

    @KafkaListener(topics = "client")
    public void consumer(String message) {
        log.info("********** MENSAJE RECIBIDO {} ***********",message);
    }

}
