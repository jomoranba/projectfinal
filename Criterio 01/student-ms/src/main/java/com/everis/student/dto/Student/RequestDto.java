package com.everis.student.dto.Student;

import lombok.Data;

import java.util.Date;

@Data
public class RequestDto {

    private String documentNumber;

    private String documentType;

    private String age;

    private String university;

    private String civil_status;

    private Boolean status;

    private Date birth_date;

    private String address;

    private String gender;

}
