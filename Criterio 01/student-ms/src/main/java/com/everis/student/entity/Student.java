package com.everis.student.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Data
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String documentNumber;

    private String documentType;

    private String age;

    private String university;

    private String civil_status;

    private Boolean status;

    private Date birth_date;

    private String address;

    private String gender;

}
