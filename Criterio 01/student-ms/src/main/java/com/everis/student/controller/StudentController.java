package com.everis.student.controller;

import com.everis.student.dto.Student.*;
import com.everis.student.entity.Student;
import com.everis.student.mapper.StudentMapper;
import com.everis.student.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
public class StudentController {

    @Autowired
    private StudentService studentService;

    @Autowired
    private StudentMapper studentMapper;

    @GetMapping("/students")
    public List<DefaultDto> getAllStudent(){
        return studentMapper.mapEntityListToDefaultDto(studentService.getAllStudent());
    }

    @GetMapping("/student2/{document_number}")
    @ResponseStatus(HttpStatus.OK)
    public DefaultDto getByDocumentNumber(@PathVariable String document_number){
        return studentMapper.mapEntityToDefaultDto(studentService.getByDocument_Number(document_number));
    }


    @GetMapping("/student/{id}")
    public ResponseGetByIdDto getStudentById(@PathVariable Long id){
        return studentMapper.mapEntityToResponseGetById(studentService.getStudentById(id));
    }

    @PostMapping("/student")
    public ResponseDto saveStudent(@RequestBody RequestDto requestStudent){
        return studentMapper.mapEntityToResponseDto(studentService.saveStudent(studentMapper.mapResquestDtoToEntity(requestStudent)));
    }

    @PutMapping("/student/{id}")
    public ResponseUpdateDto updateStudent(@PathVariable Long id, @RequestBody RequestDto requestDto){
        if(studentService.getStudentById(id) == null)
            log.info("No existe");
        return studentMapper.mapEntityToResponseUpdateDto(studentService.saveStudent(studentMapper.mapResponseUpdateDtoToEntity(requestDto,id)));
    }

    @DeleteMapping("/student/{id}")
    public void deleteStudent(@PathVariable Long id){
        studentService.deleteStudent(id);
    }

}
