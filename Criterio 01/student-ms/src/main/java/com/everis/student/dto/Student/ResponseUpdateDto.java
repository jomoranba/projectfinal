package com.everis.student.dto.Student;

import lombok.Data;
import org.jetbrains.annotations.NotNull;

import java.util.Date;

@Data
public class ResponseUpdateDto {

    private Long codigo;

    public String tipoDocumento;

    public String numeroDocumento;

    public String edad;

    public String universidad;

    public String estadoCivil;

    public Boolean estado;

    public Date fechaNacimiento;

    public String direccion;

    public String genero;

}
