package com.everis.student.repository;

import com.everis.student.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student, Long> {

    @Query(value = "select * from student where document_number = ?1", nativeQuery = true)
    Student findByDocumentNumber(String document_number);

}
