package com.everis.student.service;

import com.everis.student.entity.Student;

import java.util.List;

public interface IStudentService {

    /*public List<Student> getByDocument_Number();*/

    public Student getByDocument_Number(String document_number);

    public List<Student> getAllStudent();

    public Student getStudentById(Long id);

    public Student saveStudent(Student student);

    public void deleteStudent(Long id);
}
