package com.everis.student.mapper;

import com.everis.student.dto.Student.*;
import com.everis.student.entity.Student;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface StudentMapper {

    @Mapping(source = "documentType", target = "documentType")
    @Mapping(source = "documentNumber", target = "documentNumber")
    @Mapping(source = "age", target = "age")
    @Mapping(source = "university", target = "university")
    @Mapping(source = "civil_status", target = "civil_status")
    @Mapping(source = "birth_date", target = "birth_date")
    @Mapping(source = "address", target = "address")
    @Mapping(source = "gender", target = "gender")
    Student mapResquestDtoToEntity(RequestDto requestDto);

    @Mapping(source = "id", target = "id")
    @Mapping(source = "requestDto.documentType", target = "documentType")
    @Mapping(source = "requestDto.documentNumber", target = "documentNumber")
    @Mapping(source = "requestDto.age", target = "age")
    @Mapping(source = "requestDto.university", target = "university")
    @Mapping(source = "requestDto.civil_status", target = "civil_status")
    @Mapping(source = "requestDto.birth_date", target = "birth_date")
    @Mapping(source = "requestDto.address", target = "address")
    @Mapping(source = "requestDto.gender", target = "gender")
    Student mapResponseUpdateDtoToEntity(RequestDto requestDto, Long id);

    @Mapping(source = "id", target = "codigo")
    @Mapping(source = "documentType", target = "tipoDocumento")
    @Mapping(source = "documentNumber", target = "numeroDocumento")
    @Mapping(source = "age", target = "edad")
    @Mapping(source = "university", target = "universidad")
    @Mapping(source = "civil_status", target = "estadoCivil")
    @Mapping(source = "birth_date", target = "fechaNacimiento")
    @Mapping(source = "address", target = "direccion")
    @Mapping(source = "gender", target = "genero")
    ResponseDto mapEntityToResponseDto(Student student);

    @Mapping(source = "id", target = "codigo")
    @Mapping(source = "documentType", target = "tipoDocumento")
    @Mapping(source = "documentNumber", target = "numeroDocumento")
    @Mapping(source = "age", target = "edad")
    @Mapping(source = "university", target = "universidad")
    @Mapping(source = "civil_status", target = "estadoCivil")
    @Mapping(source = "birth_date", target = "fechaNacimiento")
    @Mapping(source = "address", target = "direccion")
    @Mapping(source = "gender", target = "genero")
    ResponseUpdateDto mapEntityToResponseUpdateDto(Student student);

    @Mapping(source = "documentType", target = "tipoDocumento")
    @Mapping(source = "documentNumber", target = "numeroDocumento")
    @Mapping(source = "age", target = "edad")
    @Mapping(source = "university", target = "universidad")
    @Mapping(source = "civil_status", target = "estadoCivil")
    @Mapping(source = "birth_date", target = "fechaNacimiento")
    @Mapping(source = "address", target = "direccion")
    @Mapping(source = "gender", target = "genero")
    ResponseGetByIdDto mapEntityToResponseGetById(Student student);

    /*@Mapping(source = "document_type", target = "tipoDocumento")
    @Mapping(source = "document_number", target = "numeroDocumento")
    @Mapping(source = "age", target = "edad")
    @Mapping(source = "university", target = "universidad")
    @Mapping(source = "civil_status", target = "estadoCivil")
    @Mapping(source = "birth_date", target = "fechaNacimiento")
    @Mapping(source = "address", target = "direccion")
    @Mapping(source = "gender", target = "genero")
    ResponseGetByCodumentNumberDto mapEntityToResponseGetById(Student student);*/

    @Mapping(source = "id", target = "codigo")
    @Mapping(source = "documentType", target = "tipoDocumento")
    @Mapping(source = "documentNumber", target = "numeroDocumento")
    @Mapping(source = "age", target = "edad")
    @Mapping(source = "university", target = "universidad")
    @Mapping(source = "civil_status", target = "estadoCivil")
    @Mapping(source = "birth_date", target = "fechaNacimiento")
    @Mapping(source = "address", target = "direccion")
    @Mapping(source = "gender", target = "genero")
    DefaultDto mapEntityToDefaultDto(Student student);

    List<DefaultDto> mapEntityListToDefaultDto(List<Student> students);

}
