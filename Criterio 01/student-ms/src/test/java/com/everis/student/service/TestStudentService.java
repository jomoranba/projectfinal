package com.everis.student.service;

import com.everis.student.entity.Student;
import com.everis.student.repository.StudentRepository;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class TestStudentService {

    @InjectMocks
    private StudentService studentService;

    @Mock
    private StudentRepository studentRepository;

    @Test
    public void testGetAllSuccessfully(){
        Student student = new Student();
        student.setId(1L);
        student.setDocumentType("DNI");
        student.setDocumentNumber("73448595");
        student.setAge("18");
        student.setUniversity("UPAO");
        student.setCivil_status("Soltero");
        student.setStatus(Boolean.TRUE);
        student.setBirth_date(new Date(2021,06,8));
        student.setAddress("asedqwe");
        student.setGender("Masculino");

        List<Student> studentList = List.of(student);

        Mockito.when(studentRepository.findAll()).thenReturn(studentList);

        Iterable<Student> studentIterable = studentService.getAllStudent();

        Assertions.assertThat(studentIterable).hasSize(1);
        Assertions.assertThat(studentIterable.iterator().next()).isEqualTo(student);

    }

    @Test
    public void testGetByIdSuccessfully(){
        Student student = new Student();
        student.setId(1L);
        student.setDocumentType("DNI");
        student.setDocumentNumber("73448595");
        student.setAge("18");
        student.setUniversity("UPAO");
        student.setCivil_status("Soltero");
        student.setStatus(Boolean.FALSE);
        student.setBirth_date(new Date());
        student.setAddress("asedqwe");
        student.setGender("Masculino");

        Optional<Student> studentOptional = Optional.of(student);

        Mockito.when(studentRepository.findById(Mockito.anyLong())).thenReturn(studentOptional);

        Student studentOp = studentService.getStudentById(1L);

        Assertions.assertThat(student).isEqualTo(student);

    }

    @Test
    public void testSaveSuccessfully(){
        Student student = new Student();
        student.setId(1L);

        student.setDocumentType("DNI");
        student.setDocumentNumber("73448595");
        student.setAge("18");
        student.setUniversity("UPAO");
        student.setCivil_status("Soltero");
        student.setStatus(Boolean.TRUE);
        student.setBirth_date(new Date());
        student.setAddress("asedqwe");
        student.setGender("Masculino");



        Mockito.when(studentRepository.save(Mockito.refEq(student))).thenReturn(student);

        Assertions.assertThat(studentService.saveStudent(student)).isEqualTo(student);
    }

    @Test
    public void testRemoveSuccessfully(){
        studentService.deleteStudent(99L);
        Mockito.verify(studentRepository, Mockito.times(1)).deleteById(Mockito.anyLong());
    }

    @Test
    public void testGetAllDocumentNumber(){
        Student student = new Student();
        student.setId(1L);
        student.setDocumentType("DNI");
        student.setDocumentNumber("73448595");
        student.setAge("18");
        student.setUniversity("UPAO");
        student.setCivil_status("Soltero");
        student.setStatus(Boolean.TRUE);
        student.setBirth_date(new Date());
        student.setAddress("asedqwe");
        student.setGender("Masculino");

        Mockito.when(studentRepository.findByDocumentNumber(Mockito.refEq("73448595"))).thenReturn(student);

        Student studentServ = studentService.getByDocument_Number("73448595");

        Assertions.assertThat(studentServ).isEqualTo(student);
    }


}
