package com.everis.student.controller;

import com.everis.student.dto.Student.*;
import com.everis.student.entity.Student;
import com.everis.student.mapper.StudentMapperImpl;
import com.everis.student.service.StudentService;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class TestStudentController {

    @InjectMocks
    private StudentController studentController;

    @Mock
    private StudentService studentService;

    @Mock
    private StudentMapperImpl studentMapper;

    @Test
    public void testGetAllSuccessfully(){
        Student student = new Student();
        student.setId(1L);
        student.setDocumentType("DNI");
        student.setDocumentNumber("73448595");
        student.setAge("18");
        student.setUniversity("UPAO");
        student.setCivil_status("Soltero");
        student.setStatus(Boolean.TRUE);
        student.setBirth_date(new Date());
        student.setAddress("asedqwe");
        student.setGender("Masculino");

        List<Student> studentList = List.of(student);

        Mockito.when(studentService.getAllStudent()).thenReturn(studentList);

        DefaultDto defaultDto = new DefaultDto();
        defaultDto.setCodigo(1L);
        defaultDto.setTipoDocumento("DNI");
        defaultDto.setNumeroDocumento("73448595");
        defaultDto.setEdad("18");
        defaultDto.setUniversidad("UPAO");
        defaultDto.setEstadoCivil("Soltero");
        defaultDto.setEstado(Boolean.TRUE);
        defaultDto.setFechaNacimiento(new Date());
        defaultDto.setDireccion("asedqwe");
        defaultDto.setGenero("Masculino");

        Mockito.when(studentMapper.mapEntityListToDefaultDto(Mockito.refEq(studentList))).thenReturn(List.of(defaultDto));

        Assertions.assertThat(studentController.getAllStudent()).isEqualTo(List.of(defaultDto));
    }

    @Test
    public void testGetAllWhenListEmpty(){
        Mockito.when(studentService.getAllStudent()).thenReturn(new ArrayList<>());

        Assertions.assertThat(studentController.getAllStudent()).isEmpty();
    }

    @Test
    public void testInsertSuccessfully(){
        RequestDto request = new RequestDto();
        request.setDocumentType("DNI");
        request.setDocumentNumber("73448595");
        request.setAge("18");
        request.setUniversity("UPAO");
        request.setCivil_status("Soltero");
        request.setStatus(Boolean.TRUE);
        request.setBirth_date(new Date());
        request.setAddress("asedqwe");
        request.setGender("Masculino");

        Student student = new Student();
        student.setDocumentType("DNI");
        student.setDocumentNumber("73448595");
        student.setAge("18");
        student.setUniversity("UPAO");
        student.setCivil_status("Soltero");
        student.setStatus(Boolean.TRUE);
        student.setBirth_date(new Date());
        student.setAddress("asedqwe");
        student.setGender("Masculino");

        Student studentSave = new Student();
        studentSave.setId(180L);
        studentSave.setDocumentType("DNI");
        studentSave.setDocumentNumber("73448595");
        studentSave.setAge("20");
        studentSave.setUniversity("UPN");
        studentSave.setCivil_status("Soltero");
        studentSave.setStatus(Boolean.TRUE);
        studentSave.setBirth_date(new Date());
        studentSave.setAddress("asedqwe");
        studentSave.setGender("Masculino");

        ResponseDto responseDto = new ResponseDto();
        responseDto.setTipoDocumento("DNI");
        responseDto.setNumeroDocumento("73448595");
        responseDto.setEdad("20");
        responseDto.setUniversidad("UPN");
        responseDto.setEstadoCivil("Soltero");
        responseDto.setEstado(Boolean.TRUE);
        responseDto.setFechaNacimiento(new Date());
        responseDto.setDireccion("asedqwe");
        responseDto.setGenero("Masculino");

        Mockito.when(studentMapper.mapResquestDtoToEntity(Mockito.refEq(request))).thenReturn(student);
        Mockito.when(studentService.saveStudent(Mockito.refEq(student))).
                thenReturn(studentSave);
        Mockito.when(studentMapper.mapEntityToResponseDto(Mockito.refEq(studentSave))).thenReturn(responseDto);

        Assertions.assertThat(studentController.saveStudent(request)).isEqualTo(responseDto);
    }

    @Test
    public void testGetByIdSuccessfully(){
        Student student = new Student();
        student.setId(1L);
        student.setDocumentType("DNI");
        student.setDocumentNumber("73448595");
        student.setAge("18");
        student.setUniversity("UPAO");
        student.setCivil_status("Soltero");
        student.setStatus(Boolean.TRUE);
        student.setBirth_date(new Date());
        student.setAddress("asedqwe");
        student.setGender("Masculino");

        Mockito.when(studentService.getStudentById(1L)).thenReturn(student);

        ResponseGetByIdDto responseGetByIdDto = new ResponseGetByIdDto();
        responseGetByIdDto.setTipoDocumento("DNI");
        responseGetByIdDto.setNumeroDocumento("73448595");
        responseGetByIdDto.setEdad("20");
        responseGetByIdDto.setUniversidad("UPN");
        responseGetByIdDto.setEstadoCivil("Soltero");
        responseGetByIdDto.setEstado(Boolean.TRUE);
        responseGetByIdDto.setFechaNacimiento(new Date());
        responseGetByIdDto.setDireccion("asedqwe");
        responseGetByIdDto.setGenero("Masculino");

        Mockito.when(studentMapper.mapEntityToResponseGetById(student)).thenReturn(responseGetByIdDto);

        Assertions.assertThat(studentController.getStudentById(1L)).isEqualTo(responseGetByIdDto);
    }

    @Test()
    public void testGetByIdWhenIdNull(){
        studentController.getStudentById(null);
    }

    @Test()
    public void testGetByIdWhenIdZero(){
        studentController.getStudentById(0L);
    }

    @Test
    public void testFindByInsertedTodaySuccessfully(){
        Student student = new Student();
        student.setId(1L);
        student.setDocumentType("DNI");
        student.setDocumentNumber("73448595");
        student.setAge("18");
        student.setUniversity("UPAO");
        student.setCivil_status("Soltero");
        student.setStatus(Boolean.TRUE);
        student.setBirth_date(new Date());
        student.setAddress("asedqwe");
        student.setGender("Masculino");

        Mockito.when(studentService.getByDocument_Number("73448595")).thenReturn(student);

        DefaultDto defaultDto = new DefaultDto();
        defaultDto.setCodigo(1L);
        defaultDto.setTipoDocumento("DNI");
        defaultDto.setNumeroDocumento("73448595");
        defaultDto.setEdad("18");
        defaultDto.setUniversidad("UPAO");
        defaultDto.setEstadoCivil("Soltero");
        defaultDto.setEstado(Boolean.TRUE);
        defaultDto.setFechaNacimiento(new Date());
        defaultDto.setDireccion("asedqwe");
        defaultDto.setGenero("Masculino");

        Mockito.when(studentMapper.mapEntityToDefaultDto(student)).thenReturn(defaultDto);

        Assertions.assertThat(studentController.getByDocumentNumber("73448595")).isEqualTo(defaultDto);

    }

    @Test
    public void testUpdateSuccesfully(){
        RequestDto request = new RequestDto();
        request.setDocumentType("DNI");
        request.setDocumentNumber("73448595");
        request.setAge("18");
        request.setUniversity("UPAO");
        request.setCivil_status("Soltero");
        request.setStatus(Boolean.TRUE);
        request.setBirth_date(new Date());
        request.setAddress("asedqwe");
        request.setGender("Masculino");

        Student student = new Student();
        student.setId(1L);
        student.setDocumentType("DNI");
        student.setDocumentNumber("73448595");
        student.setAge("18");
        student.setUniversity("UPAO");
        student.setCivil_status("Soltero");
        student.setStatus(Boolean.TRUE);
        student.setBirth_date(new Date());
        student.setAddress("asedqwe");
        student.setGender("Masculino");

        Student studentUpdate = new Student();
        studentUpdate.setId(1L);
        studentUpdate.setDocumentType("DNI");
        studentUpdate.setDocumentNumber("73448595");
        studentUpdate.setAge("18");
        studentUpdate.setUniversity("UPN");
        studentUpdate.setCivil_status("Soltero");
        studentUpdate.setStatus(Boolean.TRUE);
        studentUpdate.setBirth_date(new Date());
        studentUpdate.setAddress("asedqwe");
        studentUpdate.setGender("Masculino");

        ResponseUpdateDto responseUpdateDto = new ResponseUpdateDto();
        responseUpdateDto.setCodigo(1L);
        responseUpdateDto.setTipoDocumento("DNI");
        responseUpdateDto.setNumeroDocumento("73448595");
        responseUpdateDto.setEdad("18");
        responseUpdateDto.setUniversidad("UPN");
        responseUpdateDto.setEstadoCivil("Soltero");
        responseUpdateDto.setEstado(Boolean.TRUE);
        responseUpdateDto.setFechaNacimiento(new Date());
        responseUpdateDto.setDireccion("asedqwe");
        responseUpdateDto.setGenero("Masculino");

        Mockito.when(studentService.getStudentById(1L)).thenReturn(student);
        Mockito.when(studentMapper.mapResponseUpdateDtoToEntity(request,1L)).thenReturn(studentUpdate);
        Mockito.when(studentMapper.mapEntityToResponseUpdateDto(studentUpdate)).thenReturn(responseUpdateDto);
        Mockito.when(studentService.saveStudent(Mockito.refEq(studentUpdate))).thenReturn(studentUpdate);

        Assertions.assertThat(studentController.updateStudent(1L,request)).isEqualTo(responseUpdateDto);
    }

}
