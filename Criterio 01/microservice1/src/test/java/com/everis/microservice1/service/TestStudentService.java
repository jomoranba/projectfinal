package com.everis.microservice1.service;

import com.everis.microservice1.model.Student;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class TestStudentService {

    @InjectMocks
    private StudentService studentService;

    @Mock
    private RestTemplate restTemplate;

    @Test
    public void givenMockingIsDoneByMockito_whenGetIsCalled_shouldReturnMockedObject() {
        Student student = new Student();
        student.setCodigo(1L);
        student.setTipoDocumento("DNI");
        student.setNumeroDocumento("73448595");
        student.setEdad("18");
        student.setUniversidad("UPAO");
        student.setEstadoCivil("Soltero");
        student.setEstado(Boolean.TRUE);
        student.setFechaNacimiento(new Date());
        student.setDireccion("asedqwe");
        student.setGenero("Masculino");

        Mockito
                .when(restTemplate.getForEntity(
                        "http://localhost:9191/student/"+student.getCodigo(), Student.class))
          .thenReturn(new ResponseEntity(student, HttpStatus.OK));

        Student studentServ = studentService.getStudentById(student.getCodigo());
        Assert.assertEquals(student, studentServ);
    }

    @Test
    public void testGetAll(){
/*
        Student student = new Student();
        student.setCodigo(1L);
        student.setTipoDocumento("DNI");
        student.setNumeroDocumento("73448595");
        student.setEdad("18");
        student.setUniversidad("UPAO");
        student.setEstadoCivil("Soltero");
        student.setEstado(Boolean.TRUE);
        student.setFechaNacimiento(new Date());
        student.setDireccion("asedqwe");
        student.setGenero("Masculino");

        ResponseEntity<Student[]> studentList = (ResponseEntity<Student[]>) List.of(student);

        Mockito
                .when(restTemplate.getForEntity(
                        "http://localhost:9191/students", Student[].class))
                .thenReturn(new ResponseEntity(studentList, HttpStatus.OK));

        List<Student> studentServ = studentService.getStudentAll();
        Assert.assertEquals(studentList, studentServ);*/

    }

    @Test
    public void testSaveStudent(){

     /*   Student student = new Student();
        student.setTipoDocumento("DNI");
        student.setNumeroDocumento("73448595");
        student.setEdad("18");
        student.setUniversidad("UPAO");
        student.setEstadoCivil("Soltero");
        student.setEstado(Boolean.TRUE);
        student.setFechaNacimiento(new Date());
        student.setDireccion("asedqwe");
        student.setGenero("Masculino");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<Student> request = new HttpEntity<>(student, headers);

        //Mockito.when(restTemplate.postForEntity("http://localhost:9191/student", request, Student.class)).thenReturn(new ResponseEntity<>(student, HttpStatus.CREATED));
        Mockito.when(restTemplate.postForEntity(Mockito.refEq("http://localhost:9191/student"), Mockito.refEq(student), Student.class)).thenReturn(student);

        Student studentSave = studentService.saveStudent(student);

       Assertions.assertEquals(studentSave.getCodigo(), student.getCodigo());
        Assertions.assertEquals(studentSave.getTipoDocumento(), student.getTipoDocumento());*/

    }

}
