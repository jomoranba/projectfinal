package com.everis.microservice1.controller;


import com.everis.microservice1.model.Student;
import com.everis.microservice1.service.StudentService;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@RestController
@Slf4j
public class StudentController {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private StudentService studentService;

    private static final String URL_PUTANDDELETESTUDENT = "http://localhost:9191/student/";

    @GetMapping("/student/{id}")
    public Student getAE(@PathVariable Long id){
        return studentService.getStudentById(id);
    }

    @GetMapping("/all")
    public List<Student> getAll(){
        return studentService.getStudentAll();
    }

   @PostMapping("/student")
    public Student createProducts(@RequestBody Student student) {
        return studentService.saveStudent(student);
    }


    @PutMapping("/student/{id}")
    public String updateProduct(@PathVariable("id") long id, @RequestBody Student studentRequest) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<Student> entity = new HttpEntity<Student>(studentRequest,headers);
        log.info("http://localhost:9191/student/"+id);
        return restTemplate.exchange(
                URL_PUTANDDELETESTUDENT + id, HttpMethod.PUT, entity, String.class).getBody();
    }

    @DeleteMapping("/student/{id}")
    public String deleteProduct(@PathVariable("id") String id) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<Student> entity = new HttpEntity<Student>(headers);

        return restTemplate.exchange(
                URL_PUTANDDELETESTUDENT + id, HttpMethod.DELETE, entity, String.class).getBody();
    }
}
