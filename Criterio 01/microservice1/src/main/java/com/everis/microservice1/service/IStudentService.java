package com.everis.microservice1.service;

import com.everis.microservice1.model.Student;

import java.util.List;

public interface IStudentService {

    List<Student> getStudentAll();

    Student getStudentById(Long id);

    Student saveStudent(Student student);

}
