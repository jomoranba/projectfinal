package com.everis.microservice1.service;

import com.everis.microservice1.model.Student;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class StudentService implements IStudentService{

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private KafkaTemplate kafkaTemplate;

    @Value("${kafka.topic.default}")
    private String TOPIC;

    @Override
    public Student getStudentById(Long id) {
        ResponseEntity resp =
                restTemplate.getForEntity("http://localhost:9191/student/" + id, Student.class);

        if (resp.getBody() != null) {
            kafkaTemplate.send(TOPIC,new Gson().toJson(resp));
        }

        return resp.getStatusCode() == HttpStatus.OK ? (Student) resp.getBody() : null;
    }

    @Override
    public Student saveStudent(Student requestStudent) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<Student> requestEntity = new HttpEntity<>(requestStudent, headers);

        ResponseEntity resp =
                restTemplate.postForEntity("http://localhost:9191/student", requestStudent, Student.class);

        return resp.getStatusCode() == HttpStatus.OK ? (Student) resp.getBody() : null;
    }

    @Override
    public List<Student> getStudentAll(){
        ResponseEntity<Student[]> resp =
                restTemplate.getForEntity("http://localhost:9191/students", Student[].class);

        return resp.getStatusCode() == HttpStatus.OK ? Arrays.asList(resp.getBody()) : null;
    }

}
