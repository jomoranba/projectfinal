package com.everis.student.service;

import com.everis.student.controller.StudentController;
import com.everis.student.model.Student;
import com.everis.student.repository.StudentRepository;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.util.Date;

@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = StudentController.class)
@Import(StudentService.class)
public class TestStudentService {

    @MockBean
    private StudentRepository studentRepository;

    @Autowired
    private WebTestClient webTestClient;

    @Test
    public void testSaveStudent(){
        Student student = new Student();
        student.setId("1");
        student.setDocumentType("DNI");
        student.setDocumentNumber("73448595");
        student.setAge("18");
        student.setUniversity("UPAO");
        student.setCivil_status("Soltero");
        student.setStatus(Boolean.TRUE);
        student.setBirth_date(LocalDate.now());
        student.setAddress("asedqwe");
        student.setGender("Masculino");

        Mockito.when(studentRepository.save(student)).thenReturn(Mono.just(student));

        webTestClient.post()
                .uri("/student")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromObject(student))
                .exchange()
                .expectStatus().isCreated();

        Mockito.verify(studentRepository, Mockito.times(1)).save(student);
    }
}
