package com.everis.student.repository;

import com.everis.student.model.Student;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

public interface StudentRepository extends ReactiveMongoRepository<Student, String> {

    Mono<Student> findFirstByDocumentNumber(String documentNumber);
}
