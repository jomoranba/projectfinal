package com.everis.student.controller;

import com.everis.student.model.Student;
import com.everis.student.repository.StudentRepository;
import com.everis.student.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class StudentController {

    @Autowired
    private StudentService studentService;

    @Autowired
    private StudentRepository studentRepository;

    @GetMapping("/p/{message}")
    void example(@PathVariable("message") String message){
        studentService.sendMessage(message);
    }

    @GetMapping("/students")
    Flux<Student> getAllStudent(){
        return studentService.findAll();
    }

    @GetMapping("/student/documentNumber/{document_number}")
    Mono<Student> getStudentbyDocumentNumber(@PathVariable("document_number") String document_number){
        return studentService.findByDocumentNumber(document_number);
    }

    @GetMapping("/student/{id}")
    Mono<Student> getById(@PathVariable String id){
        return studentService.findById(id);
    }

    @PostMapping("/student")
    Mono<Student> saveStudent(@RequestBody Student requestStudent){
        return  studentService.save(requestStudent);
    }

    @PutMapping("/student/{id}")
    Mono<ResponseEntity<Student>> updateStudent(@PathVariable String id, @RequestBody Student requestStudent ){
        return studentService.update(id, requestStudent)
                .flatMap(student -> Mono.just(ResponseEntity.ok(student)))
                .switchIfEmpty(Mono.just(ResponseEntity.notFound().build()));
    }

    @DeleteMapping("/student/{id}")
    Mono<ResponseEntity<Student>> deleteStudent(@PathVariable String id){
        return studentService.delete(id)
                .flatMap(student -> Mono.just(ResponseEntity.ok(student)))
                .switchIfEmpty(Mono.just(ResponseEntity.notFound().build()));
    }

}
