package com.everis.student.service;

import com.everis.student.model.Student;
import com.everis.student.repository.StudentRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class StudentService implements IStudentService{

    @Autowired
    private StudentRepository studentRepository;

    @Value("${kafka.topic.default}")
    private String TOPIC;

    @Autowired
    private KafkaTemplate kafkaTemplate;

    public void sendMessage(String message){
        kafkaTemplate.send(TOPIC, message);
        log.info("Sending message {}", message);
    }

    @Bean
    public NewTopic topicAdvice(){
        return new NewTopic(TOPIC,3,(short) 1);
    }


    @Override
    public Mono<Student> save(Student student) {
        return studentRepository.save(student);
    }

    @Override
    public Mono<Student> delete(String id) {
        return studentRepository.findById(id)
                .flatMap( p -> studentRepository.deleteById(p.getId()).thenReturn(p));
    }

    @Override
    public Mono<Student> update(String id, Student student) {
        return this.studentRepository.findById(id)
                .flatMap(studentUpdate -> {
                    student.setId(id);
                    return save(student);
                })
                .switchIfEmpty(Mono.empty());
    }

    @Override
    public Flux<Student> findAll() {
        return studentRepository.findAll();
    }

    @Override
    public Mono<Student> findById(String id) {
        return studentRepository.findById(id);
    }

    @Override
    public Mono<Student> findByDocumentNumber(String document_number) {
        Mono<Student> documentNumber = studentRepository.findFirstByDocumentNumber(document_number);
        if(documentNumber != null)
            kafkaTemplate.send(TOPIC, document_number);
        return studentRepository.findFirstByDocumentNumber(document_number);
    }
}
