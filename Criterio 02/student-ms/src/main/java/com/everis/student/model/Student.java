package com.everis.student.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;

@Document
@Data
public class Student {

    @Id
    private String id;

    private String documentNumber;

    private String documentType;

    private String age;

    private String university;

    private String civil_status;

    private Boolean status;

    private LocalDate birth_date;

    private String address;

    private String gender;
}
