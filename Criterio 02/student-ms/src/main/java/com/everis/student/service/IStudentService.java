package com.everis.student.service;

import com.everis.student.model.Student;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface IStudentService {

    public Mono<Student> save(Student student) ;

    public Mono<Student> delete(String id);

    public Mono<Student> update(String id, Student student);

    public Flux<Student> findAll();

    public Mono<Student> findById(String id);

    public Mono<Student> findByDocumentNumber(String documentNumber);

}
