package com.everis.notification.controller;

import com.everis.notification.model.Message;
import com.everis.notification.repository.MessageRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@RestController
public class StudentController {

    @Autowired
    private MessageRepository messageRepository;

    @GetMapping("/view")
    Flux<Message> save(){
        return messageRepository.findAll();
    }
}
