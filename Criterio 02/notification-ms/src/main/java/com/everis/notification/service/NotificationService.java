package com.everis.notification.service;

import com.everis.notification.model.Message;
import com.everis.notification.repository.MessageRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class NotificationService {

    @Autowired
    private MessageRepository messageRepository;

    @KafkaListener(topics = "student")
    public void consumer(String message) {
        var rst = "El cliente con DNI:"+message + " se encontró en la Base de datos no relacional."
                ;
        Message message1 = new Message();
        message1.setMessage(rst);
        messageRepository.save(message1).subscribe();
        log.info(rst);
    }

}
